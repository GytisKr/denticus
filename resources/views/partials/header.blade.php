<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @include('partials.head')
<body>
    <div id="app">
        <div class="mobile-top-bar">
            <div class="mobile-logo">
                <a href="{{ URL::to('/') . '/' . app()->getLocale() }}">
                    <img src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.description') }}">
                </a>
            </div>
        </div>
        <div class="hamburger">
            <span></span>
            <span></span>
            <span></span>
        </div>
            <header>
            <div class="navigation">
                <div>
                    <div class="logo">
                        <a href="{{ URL::to('/') . '/' . app()->getLocale() }}">
                            <img src="{{ Voyager::image(setting('site.logo')) }}" alt="{{ setting('site.description') }}">
                        </a>
                    </div>
                    <div id="nav">
                        {{ menu(app()->getLocale(), 'menus.home') }}
                        <a data-fancybox="" href="https://www.youtube.com/watch?v=gUuZwKvfHiU">
                          </a>
                    </div>
                </div>
            </div>
        </header>
        <div id="register" class="is-closed slide-left">

            <div class="register-block">
                <a class="close-reg-form toggle-register" href="javascript:;">×</a>
                <h3>Registracija vizitui</h3>
                <form id="registration-form" action="">
                <input type="hidden" name="sheet-id" value="7254589213828996">
                <input required="" name="name" id="name" class="form-input" type="text" placeholder="Jūsų vardas">
                <input required="" name="phone" id="phone" class="form-input" type="text" placeholder="Telefono numeris">
                <select required="" class="form-input" name="time" id="form-time">
                    <option value="" selected="" disabled="" hidden="">Pasirinkite laiką</option>
                    <option value="8:00 - 12:00">8:00 - 12:00</option>
                    <option value="12:00 - 16:00">12:00 - 16:00</option>
                    <option value="16:00 - 20:00">16:00 - 20:00</option>
                </select>
                <select required="" class="form-input" name="purpose" id="form-goal">
                    <option value="" selected="" disabled="" hidden="">Vizito tikslas</option>
                    <option value="Esu naujas(-a) pacientas(-ė)">Esu naujas(-a) pacientas(-ė)</option>
                    <option value="Periodiškas patikrinimas">Periodiškas patikrinimas</option>
                    <option value="Konsultacija">Konsultacija</option>
                    <option value="Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)">Kita (pvz.: Skauda dantį, konsultacija dėl gydymo)</option>
                </select>
                <select required="" class="form-input" name="service" id="form-service">
                    <option value="" selected="" disabled="" hidden="">Pasirinkite paslaugą</option>
                    <option value="Dantų plombavimas">Dantų plombavimas</option>
                    <option value="Profesionali burnos higiena">Profesionali burnos higiena</option>
                    <option value="Dantų balinimas">Dantų balinimas</option>
                    <option value="Bruksizmo gydymas">Bruksizmo gydymas</option>
                    <option value="Dantų implantacija">Dantų implantacija</option>
                    <option value="Dantų protezavimas">Dantų protezavimas</option>
                    <option value="Dantų laminatės">Dantų laminatės</option>
                    <option value="Estetinis plombavimas">Estetinis plombavimas</option>
                    <option value="Žandikaulio sąnario gydymas">Žandikaulio sąnario gydymas</option>
                    <option value="Ortodontinis gydymas">Ortodontinis gydymas</option>
                    <option value="Nematomos tiesinimo kapos">Nematomos tiesinimo kapos</option>
                    <option value="Endodontinis gydymas mikroskopu">Endodontinis gydymas mikroskopu</option>
                    <option value="Periodontologija, dantenų recesijų gydymas">Periodontologija, dantenų recesijų gydymas</option>
                    <option value="Burnos chirurgija, dantų šalinimas">Burnos chirurgija, dantų šalinimas</option>
                    <option value="Tyrimai ir diagnostika">Tyrimai ir diagnostika</option>
                </select>
                <textarea required="" class="form-input" name="fmessage" id="form-comment" cols="30" rows="5" placeholder="Papildoma informacija"></textarea>
                  <button id="register-submit" type="submit" name="button" class="btn-register">{{ __("msg.register.approve") }}<div class="arrow"><img src="/storage/settings/November2020/arrow-dark.png" alt=""></div></button>
                  <div class="success-message" style="display: none;">
                    <h3>Registracija sėkminga!<br>Netrukus su jumis susisieksime!</h3>
                  </div>
                </form>
            </div>
        </div>