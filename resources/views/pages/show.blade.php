@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="page-inner">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ $page->title }}
                    </h1>
                </div>
                <div class="column is-12">
                    <div class="page-content">
                        {!! $page->body !!}
                    </div>
                </div>
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection