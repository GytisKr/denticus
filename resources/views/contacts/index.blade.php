@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="contacts">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.contacts.title') }}
                    </h1>
                </div>
            </div>
            <div class="contact-info">
                <div class="columns">
                    <div class="column is-6">
                    <p><strong>{{ __('msg.contacts.tel') }}: </strong><a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a></p>
                    <p><strong>{{ __('msg.contacts.email') }}: </strong><a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></p>
                    <p><strong>{{ __('msg.contacts.adress') }}: </strong><a target="_blank" href="{{ setting('site.map_link') }}">{{ setting('site.adress') }}</a></p>
                    </div>
                    <div class="column is-6">
                        <p><strong>{{ __('msg.contacts.work') }}</strong></p>
                        <p>{{ setting('site.hours') }}</p>
                        <ul class="work-list">
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="empty"></li>
                            <li class="empty"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="about-us">
                <div class="columns">
                    <div class="column is-12">
                        <p><strong>{{ __('msg.contacts.about') }}:</strong></p>
                        {!! setting('site.about_us_footer') !!}
                    </div>
                </div>
                <div class="contacts-photos">
                    <div class="columns">
                        <div class="column is-3">
                            <a href="{{ Voyager::image(setting('site.image1')) }}" data-fancybox="images" data-caption="{{setting('site.title')}} {{setting('site.description')}}">
                                <img src="{{ Voyager::image(setting('site.image1')) }}" alt="" />
                            </a>
                        </div>
                        <div class="column is-3">
                            <a href="{{ Voyager::image(setting('site.image2')) }}" data-fancybox="images" data-caption="{{setting('site.title')}} {{setting('site.description')}}">
                                <img src="{{ Voyager::image(setting('site.image2')) }}" alt="" />
                            </a>
                        </div>
                        <div class="column is-3">
                            <a href="{{ Voyager::image(setting('site.image3')) }}" data-fancybox="images" data-caption="{{setting('site.title')}} {{setting('site.description')}}">
                                <img src="{{ Voyager::image(setting('site.image3')) }}" alt="" />
                            </a>
                        </div>
                        <div class="column is-3">
                            <a href="{{ Voyager::image(setting('site.image4')) }}" data-fancybox="images" data-caption="{{setting('site.title')}} {{setting('site.description')}}">
                                <img src="{{ Voyager::image(setting('site.image4')) }}" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1156.256694779134!2d24.340793190634848!3d55.72930205363892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e631f55446dd47%3A0x2f1bd7cce8ee8079!2sPalangos%20g.%2047%2C%20Panev%C4%97%C5%BEys%2035193!5e0!3m2!1sen!2slt!4v1605174706255!5m2!1sen!2slt" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
                <div class="copyright">
                <span>&copy; {{ setting('site.title') }} 2020. {{ __('msg.contacts.rights') }} </span><span>{{ __('msg.contacts.created') }} <a href="https://expertmedia.lt/" target="_blank"><img style="top: 5px;" src="/storage/settings/November2020/expertmedia.png" alt=""></a></span>
            </div>
        </div>
    </section>
</div>

@endsection