@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="testimonials">
            <div class="columns">
                <div class="column is-6">
                    <h1 class="section-title">
                        {{ __('msg.testimonials.title') }}
                    </h1>
                </div>
                <div class="column is-6">
                    <div class="follow-us">
                        <div class="info">
                            <h2>{{ __("msg.testimonials.leave") }}</h2>
                            <h3>{{ __("msg.testimonials.choose") }}</h3>
                        </div>
                        <div class="logos">
                            <div class="logo">
                                <a href="#">
                                    <div class="logo-icon">
                                        <img src="/storage/settings/November2020/google-small.png" alt="">
                                    </div>
                                </a> 
                                <p>Google</p>
                            </div>
                            <div class="logo">
                                <a href="#">
                                    <div class="logo-icon">
                                        <img src="/storage/settings/November2020/facebook-small.png" alt="">
                                    </div>
                                </a>
                                <p>Facebook</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                <div class="column is-6">
                    <img class="testimonial-logo" src="/storage/settings/November2020/google.png" alt="">
                    <div class="testimonial-block google-testimonials">
                        @foreach ($gotestimonials as $gotestimonial)
                            <div class="testimonial-item">
                                <div class="about">
                                    <img src="{{ Voyager::image($gotestimonial->image) }}" alt="{{ $gotestimonial->title }}">
                                    <h1>{{$gotestimonial->title}}</h1>
                                </div>
                                <div class="content">
                                    {{$gotestimonial->body}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="column is-6">
                    <img class="testimonial-logo" src="/storage/settings/November2020/facebook.png" alt="">
                    <div class="testimonial-block facebook-testimonials">
                        @foreach ($fbtestimonials as $fbtestimonial)
                        <div class="testimonial-item">
                            <div class="about">
                                <img src="{{ Voyager::image($fbtestimonial->image) }}" alt="{{ $fbtestimonial->title }}">
                                <h1>{{$fbtestimonial->title}}</h1>
                            </div>
                            <div class="content">
                                {{$fbtestimonial->body}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection