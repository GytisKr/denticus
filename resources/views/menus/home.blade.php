<div id="menu">
    @php
        if (Voyager::translatable($items)) {
            $items = $items->load('translations');
        }
    @endphp
    @foreach ($items as $item)
        @php
            $originalItem = $item;
            if (Voyager::translatable($item)) {
                $item = $item->translate($options->locale);
            }
            $isActive = null;
            // Check if link is current
            if(url($item->link()) == url()->current()){
                $isActive = 'active';
            }
        @endphp
        <div class="nav-item {{ $isActive }} @if(!$originalItem->children->isEmpty()) has-children @endif">
            @if (\Request::route()->getName() == 'homepage')
                <a href="{{ app()->getLocale() }}{{ $item->url }}">
            @else
                <a href="/{{ app()->getLocale() }}{{ $item->url }}">
            @endif
                <span>{{ $item->title }}</span>
            </a>
            @if(!$originalItem->children->isEmpty())
                <i class="fas fa-sort-down"></i>
            @endif
            @if(!$originalItem->children->isEmpty())
                {{-- @include('voyager::menu.default', ['items' => $originalItem->children, 'options' => $options]) --}}
                <ul class="dropdown-menu">
                    @foreach ($originalItem->children as $children)
                        <li>
                            @if (\Request::route()->getName() == 'homepage')
                                <a href="{{ app()->getLocale() }}{{ $children->url }}">
                            @else
                                <a href="{{ config('app.url') }}{{ app()->getLocale() }}{{ $children->url }}">
                            @endif
                                {{ $children->title }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    @endforeach

    <a id="main-register" class="toggle-register btn-register">{{ __("msg.form.register-online") }} <div class="arrow"><img src="/storage/settings/November2020/arrow.png" alt=""></div></a>

    {{-- <i class="fas fa-times close-menu"></i>     --}}
    <div class="menu-contacts">
        <a href="tel:{{setting('site.phone')}}">{{setting('site.phone')}}</a>
        <a href="mailto:{{setting('site.email')}}">{{setting('site.email')}}</a>
    </div>

    @php
        $languages = config('voyager.multilingual.locales');
    @endphp
    
    <div class="languages">
        @foreach ($languages as $language)
        <div class="lang-item">
            <a href="{{ URL::to('/') }}/{{ $language }}">{{ $language }}</a>
        </div>
        @endforeach
    </div>

</div>

    