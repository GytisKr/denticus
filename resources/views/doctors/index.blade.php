@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="doctors">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.doctors.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                @foreach ($doctors as $doctor)
                    <div class="column is-4">
                        <div class="doctor-block">
                            <div class="doctor-image tap">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                    <div class="tap-icon"></div>
                                    <img src="{{ Voyager::image($doctor->image) }}" alt="{{ $doctor->title }}">
                                </a>
                            </div>
                            <div class="doctor-info">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                    <h3>{{ $doctor->title }}</h3>
                                </a>
                                <p>{{ $doctor->position }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection