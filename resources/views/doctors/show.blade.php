@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <a href="javascript:history.back()" class="back"><img src="/storage/settings/November2020/gold-arrow.svg" alt="">{{ __('msg.global.back') }}</a>
        <div id="doctor-inner">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.doctors.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="doctor-info">
                        <div class="doctor-title">
                            <strong>{{ $doctor->title }} - </strong> {{ $doctor->position }}
                        </div>
                        <div class="doctor-content">
                            <img src="{{ Voyager::image($doctor->image) }}" alt="{{ $doctor->title }}">
                            {!! $doctor->body !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ask-question">
            <h4><strong>{{ __('msg.doctors.ask') }}</strong></h4>
            <div id="{{ $doctor->active }}-online" class="doctor-activity active">
                <p>{{ __("msg.doctors.online") }}</p>
            </div>
            <div id="{{ $doctor->active }}-offline" class="doctor-activity inactive">
                <p>{{ __("msg.doctors.offline") }}</p>
            </div>

            <div class="ask-flex">
                <div class="video" style="background-image:url('{{ Voyager::image($doctor->image) }}')">
                </div>
                <div class="form">
                    <form id="ask-form" action="">
                        <input type="hidden" name="sheet-id" value="5352617707628420">
                        <div class="columns">
                            <div class="column is-12">
                                <input required="" name="ask_name" id="ask_name" class="form-input" type="text" placeholder="Jūsų vardas, pavardė">
                            </div>
                            <div class="column is-6">
                                <input required="" name="ask_email" id="ask_email" class="form-input" type="text" placeholder="El. paštas">
                            </div>
                            <div class="column is-6">
                                <input required="" name="ask_phone" id="ask_phone" class="form-input" type="text" placeholder="Tel. numeris">
                                <input required="" name="ask_doctor" id="ask_doctor" class="form-input" type="hidden" value="{{$doctor->title}}">
                            </div>
                            <div class="column is-12">
                                <textarea required="" class="form-input" name="ask_comment" id="ask_comment" cols="30" rows="2" placeholder="Žinutė"></textarea>
                                <button id="register-submit" type="submit" name="button" class="black-btn">Siųsti</button>
                                <div class="success-message" style="display: none;">
                                    <p>Žinutė išsiųsta<br>Netrukus su jumis susisieksime!</p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection