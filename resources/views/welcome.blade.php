@extends('layouts.app')

@section('content')

<div class="app">

    <div id="slider">
        <div class="container">
            <div class="slider-carousel">
                @foreach ($sliders as $slider)
                    <div class="slider" style="background-image: url('{{ Voyager::image($slider->img) }}')">
                        <div class="slider-item">
                            <div class="slider-content">
                                <div class="arrows">
                                    <a class="prev slick-arrow" style=""><img src="/storage/settings/November2020/slider-arrow.png" alt=""> </a>
                                    <a class="next slick-arrow" style=""><img src="/storage/settings/November2020/slider-arrow.png" alt=""> </a>
                                </div>
                                <h1>{!! $slider->header !!}</h1>
                                <h3>{!! $slider->subheader !!}</h3>
                                <div>
                                    @if ($slider->btnactive == 1)
                                        <a class="slider-btn" href="{{ $slider->btnlink }}">{{ $slider->btntxt }}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <section>
        <div id="services">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.services.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="service-list">
                        @foreach ($servicelists as $service)
                            <div class="service-item">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">{{ $service->title }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="columns">
                @foreach ($services as $service)
                    <div class="column is-12">
                        <div class="service-block">
                            <div class="service-image tap">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                                    <div class="tap-icon"></div>
                                    <img src="{{ Voyager::image($service->image) }}" alt="{{ $service->title }}">
                                </a>
                            </div>
                            <div class="service-info">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                                    <h3>{{ $service->title }}</h3>
                                </a>
                                <p>{{ $service->excerpt }}</p>
                                <div class="read-more">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">{{ __('msg.services.more') }} <img src="/storage/settings/November2020/gold-arrow.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="column is-12">
                <a class="black-btn" href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __('msg.services.url') }}">{{ __('msg.services.all') }}</a>
            </div>
        </div>
    </section>

    <section class="white-section">
        <div id="doctors">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.doctors.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                @foreach ($doctors as $doctor)
                    <div class="column is-4">
                        <div class="doctor-block">
                            <div class="doctor-image tap">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                    <div class="tap-icon"></div>
                                    <img src="{{ Voyager::image($doctor->image) }}" alt="{{ $doctor->title }}">
                                </a>
                            </div>
                            <div class="doctor-info">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                    <h3>{{ $doctor->title }}</h3>
                                </a>
                                <p>{{ $doctor->position }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="column is-12">
                <a class="black-btn" href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __('msg.doctors.url') }}">{{ __('msg.doctors.all') }}</a>
            </div>
        </div>
    </section>

    <section class="pb0">
        <div id="contacts">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.contacts.title') }}
                    </h1>
                </div>
            </div>
            <div class="contact-info">
                <div class="columns">
                    <div class="column is-6">
                    <p><strong>{{ __('msg.contacts.tel') }}: </strong><a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a></p>
                        <p><strong>{{ __('msg.contacts.email') }}: </strong><a href="mailto:{{ setting('site.email') }}">{{ setting('site.email') }}</a></p>
                        <p><strong>{{ __('msg.contacts.adress') }}: </strong><a target="_blank" href="{{ setting('site.map_link') }}">{{ setting('site.adress') }}</a></p>
                    </div>
                    <div class="column is-6">
                        <p><strong>{{ __('msg.contacts.work') }}</strong></p>
                        <p>{{ setting('site.hours') }}</p>
                        <ul class="work-list">
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="full"></li>
                            <li class="empty"></li>
                            <li class="empty"></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="about-us">
                <div class="columns">
                    <div class="column is-12">
                        <p><strong>{{ __('msg.contacts.about') }}:</strong></p>
                        {!! setting('site.about_us_footer') !!}
                    </div>
                </div>
            </div>
            <div class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1156.256694779134!2d24.340793190634848!3d55.72930205363892!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e631f55446dd47%3A0x2f1bd7cce8ee8079!2sPalangos%20g.%2047%2C%20Panev%C4%97%C5%BEys%2035193!5e0!3m2!1sen!2slt!4v1605174706255!5m2!1sen!2slt" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
                <div class="copyright">
                <span>&copy; {{ setting('site.title') }} 2020. {{ __('msg.contacts.rights') }} </span><span>{{ __('msg.contacts.created') }} <a href="https://expertmedia.lt/" target="_blank"><img style="top: 5px;" src="/storage/settings/November2020/expertmedia.png" alt=""></a></span>
            </div>
        </div>
    </section>
</div>

@endsection