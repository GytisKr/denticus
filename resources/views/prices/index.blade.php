@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="prices">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.prices.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns price-tabs">
                @foreach ($prices as $price)
                    <div class="column is-4">
                        <a href="#{{ $price->slug }}">{{ $price->title }}</a>
                    </div>
                @endforeach
            </div>
            @foreach ($prices as $price)
                <div class="panelContainer">
                    <div id="{{ $price->slug }}" class="price-panel panel">
                        <div class="price-content">
                            <table>
                                <tr>
                                    <td class="table-title">{{ $price->title }}</td>
                                    <td class="table-price">{{ __('msg.prices.price') }}</td>
                                </tr>
                            </table>
                            {!! $price->body !!}
                        </div>
                    </div>
                </div>
            @endforeach
            <p class="warning"><strong>{{ __('msg.prices.warning') }} </strong>{{ __('msg.prices.warning_txt') }}</p>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection