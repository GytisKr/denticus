@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="articles">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.articles.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="filter">
                        {{ __('msg.articles.filter') }}
                        <div class="filter-toggle">
                            <img src="/storage/settings/November2020/filter.svg" alt="">
                        </div>
                        <div class="filter-list">
                            <div class="close filter-toggle"><img src="/storage/settings/November2020/close.svg" alt=""></div>
                            <ul>
                                <li data-search="0">
                                    Visos naujienos
                                </li>
                                @foreach ($services as $service)
                                    @if ($service->articles->count())
                                        <li data-search="{{ $service->id }}">
                                            {{ $service->getTranslatedAttribute('title') }}
                                        </li>
                                    @endif 
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="columns">
                @foreach ($articles as $article)
                    <div class="column is-12 article-block active" data-services="{{ $article->getServicesIds($article->id) }}">
                        <div class="article-image">
                            <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.articles.url") }}/{{ $article->slug }}">
                                <img src="{{ Voyager::image($article->image) }}" alt="{{ $article->title }}">
                            </a>
                        </div>
                        <div class="article-info">
                            <div class="article-border">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.articles.url") }}/{{ $article->slug }}">
                                    <h3>{{ $article->title }}</h3>
                                </a>
                                <p>{{ $article->excerpt }}</p>
                                <div class="article-date">
                                    {{ $article->created_at->format('Y/m/d') }}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection