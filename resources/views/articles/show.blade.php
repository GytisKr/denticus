@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="article-inner">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="article-title">
                        {{ $article->title }}
                    </h1>
                    <div class="article-content">
                        {!! $article->body !!}
                    </div>
                    <div class="cta-btns">
                        <a class="toggle-register dark-btn">Registracija internetu</a><a class="light-btn" href="/lt/kontaktai">Kontaktai</a>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection