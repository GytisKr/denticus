@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <a href="javascript:history.back()" class="back"><img src="/storage/settings/November2020/gold-arrow.svg" alt="">{{ __('msg.global.back') }}</a>
        <div id="service-inner">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="service-title">
                        {{ $service->title }}
                    </h1>
                    <div class="service-content">
                        {!! $service->body !!}
                    </div>
                    <div class="cta-btns">
                        <a class="toggle-register dark-btn">Registracija internetu</a><a class="light-btn" href="/lt/kontaktai">Kontaktai</a>
                    </div>
                </div>
            </div>
            @if ($service->doctors->count())
                <div id="doctors">
                    <div class="columns">
                        <div class="column is-12">
                            <h1 class="service-title">
                                {{ __("msg.doctors.recomended") }}
                            </h1>
                        </div>
                    </div>
                    <div class="columns">
                        @foreach ($service->doctors as $doctor)
                            <div class="column is-3">
                                <div class="doctor-block">
                                    <div class="doctor-image tap">
                                        <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                            <div class="tap-icon"></div>
                                            <img src="{{ Voyager::image($doctor->image) }}" alt="{{ $doctor->title }}">
                                        </a>
                                    </div>
                                    <div class="doctor-info">
                                        <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.doctors.url") }}/{{ $doctor->slug }}">
                                            <h3>{{ $doctor->title }}</h3>
                                        </a>
                                        <p>{{ $doctor->position }}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection