@extends('layouts.app')

@section('content')

<div class="app">
    <section class="pb0">
        <div id="services">
            <div class="columns">
                <div class="column is-12">
                    <h1 class="section-title">
                        {{ __('msg.services.title') }}
                    </h1>
                </div>
            </div>
            <div class="columns">
                <div class="column is-12">
                    <div class="service-list">
                        @foreach ($services as $service)
                            <div class="service-item">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">{{ $service->title }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="columns">
                @foreach ($services as $service)
                    <div class="column is-12">
                        <div class="service-block">
                            <div class="service-image tap">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                                    <div class="tap-icon"></div>
                                    <img src="{{ Voyager::image($service->image) }}" alt="{{ $service->title }}">
                                </a>
                            </div>
                            <div class="service-info">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">
                                    <h3>{{ $service->title }}</h3>
                                </a>
                                <p>{{ $service->excerpt }}</p>
                                <div class="read-more">
                                <a href="{{ URL::to('/') }}/{{ app()->getLocale() }}/{{ __("msg.services.url") }}/{{ $service->slug }}">{{ __('msg.services.more') }} <img src="/storage/settings/November2020/gold-arrow.svg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @include('partials.contacts')
    </section>
</div>

@endsection