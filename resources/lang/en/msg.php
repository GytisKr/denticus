<?php

return [
  'form' => [
    'register-online' => 'Register online',
  ],
  'services' => [
    'title' => 'Services',
    'url' => 'services',
  ],
  'doctors' => [
    'title' => 'Doctors',
    'url' => 'doctors',
    'all' => 'All doctors',
  ],
];