<?php

return [
  'global' => [
    'back' => 'Grįžti',
  ],
  'form' => [
    'register-online' => 'Registracija internetu',
  ],
  'services' => [
    'title' => 'Paslaugos',
    'url' => 'paslaugos',
    'more' => 'Plačiau',
    'all' => 'Visos paslaugos',
  ],
  'doctors' => [
    'title' => 'Gydytojai',
    'url' => 'gydytojai',
    'all' => 'Visi gydytojai',
    'ask' => 'Užduoti klausimą gydytojui',
    'online' => 'Prisijungęs',
    'offline' => 'Neprisijungęs',
    'recomended' => 'Rekomenduojami gydytojai',
  ],
  'articles' => [
    'title' => 'Informatyvu',
    'url' => 'informatyvu',
    'filter' => 'Filtracija',
  ],
  'prices' => [
    'title' => 'Kainos',
    'price' => 'Kaina',
    'warning' => 'Pastaba:',
    'warning_txt' => 'Tiksli gydymo samata apskaičiuojama konsultacijos metu.  Dideliems darbams kaina koreguojama paciento naudai.',
  ],
  'testimonials' => [
    'title' => 'Atsiliepimai',
    'leave' => 'Palikti atsiliepimą',
    'choose' => 'Pasirinkite kur norit palikti atsiliepimą',
  ],
  'contacts' => [
    'title' => 'Kontaktai',
    'tel' => 'Registratūros tel.',
    'email' => 'El. paštas',
    'adress' => 'Adresas',
    'work' => 'Darbo laikas',
    'about' => 'Apie mus',
    'rights' => 'Visos teisės saugomos.',
    'created' => 'Sukūrė: ',
  ],
  'register' => [
    'approve' => 'Patvirtinti registraciją',
  ],
];