const { toArray } = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
    window.fancy = require('@fancyapps/fancybox');
    window.slick = require('slick-carousel');
} catch (e) {}

/*
Main Slider
*/

$('.slider-carousel').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 8000
});

$('.prev').click(function(){
    $('.slider-carousel').slick('slickPrev');
})
    
$('.next').click(function(){
    $('.slider-carousel').slick('slickNext');
})
  

/*
Price Tabs
*/

$(document).ready(function() {
    $('.price-tabs a').click(function(){
        $('.panel').hide();
        $('.price-tabs a.active').removeClass('active');
        $(this).addClass('active');
        var panel = $(this).attr('href');
        $(panel).fadeIn(100);
        return false;
    });
    $('.price-tabs .column:first a').click();


    /**
     * Filter articles by service id.
     */
    const articles = $('[data-services]');
    const services = $('[data-search]');

    if (articles.length && services.length) {
        
        var service_index = 0;

        for (let i = 0; i < services.length; i++) {
            const service = services[i];
            
            service.addEventListener('click', () => {
                service_index = service.dataset.search;
                showArticles(parseInt(service_index));
            });
        }

        function showArticles(id) {
            for (let i = 0; i < articles.length; i++) {
                const article = articles[i];

                /**
                 * Konvertuojam string i array
                 */
                let servicesIds = article.dataset.services;
                let idsArr = servicesIds.split(', ');
                
                function matchesId() {
                    let found = false;
                    for(const [key, value] of Object.entries(idsArr)) {
                        if (id === parseInt(value)) {
                            found = true;
                            break;
                        }
                    }
                    return found;
                }

                if (id === 0) {
                    article.classList.add('active');
                } else if(matchesId() === true) {
                    article.classList.add('active');
                } else {
                    article.classList.remove('active');
                }
            }
        }
    }

});

/**
 * Dropdown menu toggle
 */

$('.fa-sort-down').on('click', function(e) {
    $('.dropdown-menu').toggleClass("dropdown-menu-active"); //you can list several class names 
    e.preventDefault();
  });

/**
 * Main register toggle
 */

$('.toggle-register').on('click', function(e) {
    $('.slide-left').toggleClass("is-open"); //you can list several class names 
    e.preventDefault();
  });


/**
 * Filter toggle
 */

$('.filter-toggle').on('click', function(e) {
    $('.filter-list').toggleClass("filter-opened"); //you can list several class names 
    e.preventDefault();
  });

/**
 * Main menu toggle
 */

$('.hamburger').on('click', function(e) {
    $('.navigation').toggleClass("menu-is-open");
    $('.hamburger').toggleClass("x-hamburger"); //you can list several class names 
    e.preventDefault();
  });

/*
 * Doctor activity
 */

var d = new Date();
var n = d.getHours();
var w = d.getDay();

// Morning
var morningOn = document.getElementById("morning-online");
var morningOff = document.getElementById("morning-offline");

if (morningOn != null && morningOff != null) {
    if (n >= 8 && n <=12 && w != 0 && w != 6) {
        morningOn.classList.add("is-on");
    }
    else {
        morningOff.classList.add("is-on");
    }
}

// Afternoon
var afternoonOn = document.getElementById("afternoon-online");
var afternoonOff = document.getElementById("afternoon-offline");

if (afternoonOn != null && afternoonOff != null) {
    if (n >= 12 && n <=16 && w != 0 && w != 6) {
        afternoonOn.classList.add("is-on");
    }
    else {
        afternoonOff.classList.add("is-on");
    }
}

// Evening
var eveningOn = document.getElementById("evening-online");
var eveningOff = document.getElementById("evening-offline");

if (eveningOn != null && eveningOff != null) {
    if (n >= 16 && n <=20 && w != 0 && w != 6) {
        eveningOn.classList.add("is-on");
    }
    else {
        eveningOff.classList.add("is-on");
    }
}

/*
* Register forms
*/

/**
 * Forma
 */

$('#registration-form').on('submit', function(e) {
    e.preventDefault();
  
    let form = $(this);
    let formData = form.serialize();
    let successMsg = form.find('.success-message');
  
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  
    $.ajax({
      url: window.location.origin + '/lt/forma',
      method: 'POST',
      data: formData,
      success: function(res) {
  
        if (res.status) {
          successMsg.show();
  
          setTimeout(() => {
            successMsg.hide();
            form[0].reset();
          }, 4000);
        }
      },
      error: function(err) {
        console.log(err);
      }
    });
  
  });
  
  $('#ask-form').on('submit', function(e) {
    e.preventDefault();
  
    let form = $(this);
    let formData = form.serialize();
    let successMsg = form.find('.success-message');
  
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
  
    $.ajax({
      url: window.location.origin + '/lt/susisiek',
      method: 'POST',
      data: formData,
      success: function(res) {
  
        if (res.status) {
          successMsg.show();
  
          setTimeout(() => {
            successMsg.hide();
            form[0].reset();
          }, 4000);
        }
      },
      error: function(err) {
        console.log(err);
      }
    });
  
  });
  