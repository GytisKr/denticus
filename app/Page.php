<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Page extends Model
{
    use Translatable;
    protected $translatable = ['title', 'slug', 'body', 'meta_description'];

    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }
    
}
