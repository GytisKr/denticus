<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Testimonial;

class TestimonialsController extends Controller
{
    public function index()
    {
        $fbtestimonials = Testimonial::withTranslation(app()->getLocale())->where('status', 1)->where('type', "Facebook")->orderBy('created_at', 'ASC')->get();
        $gotestimonials = Testimonial::withTranslation(app()->getLocale())->where('status', 1)->where('type', "Google")->orderBy('created_at', 'ASC')->get();
        
        return view('testimonials.index', compact('fbtestimonials','gotestimonials'));
    }
}
