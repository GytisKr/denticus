<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PagesController extends Controller
{
    public function show($locale, $slug)
    {
        $page = Page::findBySlug($slug);

        return view('pages.show', compact('page'));
    }
}
