<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Service;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::withTranslation(app()->getLocale())->where('status', 1)->orderBy('created_at', 'ASC')->get();

        $services = Service::with('articles')->withTranslation(app()->getLocale())->select('id', 'title')->where('status', 1)->orderBy('created_at', 'ASC')->get();

        return view('articles.index', compact('articles', 'services'));
    }

    public function show($locale, $slug)
    {
        $article = Article::findBySlug($slug);

        return view('articles.show', compact('article'));
    }
}
