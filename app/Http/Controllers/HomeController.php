<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

use Illuminate\Http\Request;
use App\Slider;
use App\Service;
use App\Doctor;

class HomeController extends Controller
{
    public function redirect()
    {
        return redirect(app()->getLocale());    
    }

    public function index()
    {
        $sliders = Slider::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->get();
        $services = Service::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->paginate(3);
        $servicelists = Service::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->get();
        $doctors = Doctor::where('status', 1)->orderBy('order', 'ASC')->withTranslation(app()->getLocale())->paginate(6);

        return view('welcome', compact('sliders','services','doctors', 'servicelists'));
    }
}
