<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Testimonial extends Model
{
    use Translatable;
    protected $translatable = ['title', 'body', 'meta_description'];

    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }
}
