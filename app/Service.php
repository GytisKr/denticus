<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Service extends Model
{
    use Translatable;
    protected $translatable = ['title', 'slug', 'body', 'meta_description', 'excerpt'];
    
    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }
        
    public function doctors()
    {
        return $this->belongsToMany(Doctor::class);
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_service');
    }
}
