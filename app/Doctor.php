<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Doctor extends Model
{
    use Translatable;
    protected $translatable = ['title', 'slug', 'position', 'body', 'meta_description'];

    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }
}
