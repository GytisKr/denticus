<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Article extends Model
{
    use Translatable;
    protected $translatable = ['title', 'slug', 'excerpt', 'body', 'meta_description'];

    public static function findBySlug($slug)
    {
        return static::whereTranslation('slug', $slug)->firstOrFail();
    }

    public function services()
    {
        return $this->belongsToMany(Service::class, 'article_service');
    }

    public function getServicesIds($id)
    {
        $article = Article::find($id);

        $ids = [];

        foreach ($article->services as $service) {
            array_push($ids, $service->id);
        }

        return implode(', ', $ids);
    }
}
