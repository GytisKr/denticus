<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;


Route::get('/', 'HomeController@redirect');


Route::group(['prefix' => '{locale}', 'where' => ['locale' => '[a-zA-Z]{2}'], 'middleware' => 'setLocale'], function () {
    
    Route::get('/', 'HomeController@index')->name('homepage');

    Route::get('gydytojai', 'DoctorsController@index')->name('doctors.index-' . app()->getLocale());
    Route::get('gydytojai/{slug}', 'DoctorsController@show')->name('doctors.show-' . app()->getLocale());
    
    Route::get('paslaugos', 'ServicesController@index')->name('services.index-' . app()->getLocale());
    Route::get('paslaugos/{slug}', 'ServicesController@show')->name('services.show-' . app()->getLocale());

    Route::get('informatyvu', 'ArticlesController@index')->name('articles.index-' . app()->getLocale());
    Route::get('informatyvu/{slug}', 'ArticlesController@show')->name('articles.show-' . app()->getLocale());
    
    Route::get('kainos', 'PricesController@index')->name('prices.index-' . app()->getLocale());
    
    Route::get('atsiliepimai', 'TestimonialsController@index')->name('testimonials.index-' . app()->getLocale());

    Route::get('kontaktai', 'ContactsController@index')->name('contacts.index-' . app()->getLocale());
    // Route::post('kontaktai', 'ContactsController@register')->name('contacts.register-' . app()->getLocale());

    // Route::post('konsultacija', 'ContactsController@consultationsmartsheet')->name('contacts.consultationsmartsheet');
    
    /**
     * Register
     */

    Route::post('forma', 'ContactsController@register')->name('contacts.register');
    Route::post('susisiek', 'ContactsController@contactus')->name('contacts.contactus');

    /**
     * Pages
     */
    Route::get('{slug}', 'PagesController@show');

});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
